import requests

web_apps_urls = {
	'Itim': 'http://172.17.58.134:9080/itim/console/jsp/logon/Login.jsp',
	'Andes': 'http://siebelprd.vtr.cl:2080/ecommunications_ldap_esn/start.swe?SWECmd=Login&SWEPL=1&SWEBHWND=1&SRN=&SWEHo=siebelprd.vtr.cl&SWETS=1529078799',
	'ISAC': 'https://isac.custhelp.com/app/login_form/redirect/home',
	'Campus Virtual (UNIVTR)': 'http://campusvirtual.vtr.cl/univtr/login/index.php',
	'Aplicación Móvil Técnico': 'http://190.160.3.52/#!/login',
	'Sitio principal VTR': 'http://www.vtr.com',
	'Webmail VTR': 'http://m.vtr.cl/owa/',
	'Consola GCP (correos externos)': 'http://190.160.0.151:8010/DomainAdmin/e.vtr.cl/ObjectList.html?InCluster=1&domainName=e.vtr.cl',
	'Intranet': 'http://elmuro.vtr.cl/Paginas/default.aspx',
	'Adexus': 'http://www.adexus.cl',
	'Webmail Adexus': 'http://webmail.adexus.cl',
	'Reclamos': 'http://reclamos.vtr.cl:7003',
	'Mantenedor Usuarios Tango': 'http://manfact.vtr.cl/mut',
	'NDC': 'http://eps.vtr.cl/eps',
	'HR Cloud': 'http://hrcloud.vtr.cl',
	'NNOC': 'http://nnoc.vtr.cl',
	'Gecko': 'http://geko.vtr.cl/Geko/login.php',
	'Orcale E-Business Suite': 'http://ebs.andes.vtr.cl:8000/OA_HTML/AppsLocalLogin.jsp',
	'Caja única': 'http://cajaunica.vtr.cl/vtrCajasAdminWAR',
	'Helpdesk': 'http://webhelpdesk.vtr.cl',
	'Página prueba': 'https://erwerwerwer.cl'
}

shared_folders = {
	'vtrsan02': '\\\\vtrsan02\\unidad J\\',
	'vtrsan08': '\\\\vtrsan08\\unidad J\\',
}

# Conectivity test

for name, url in web_apps_urls.items():

	print('testing %s, url: %s' % (name, url))

	try:
		response = requests.get(url)
	except:
		print('There is a conectivity problem')

	with open('new_html\\%s.html' % name, 'w') as f:
		f.write(response.text)

# check_differences

	